// Commands based on https://github.com/humbertopiaia/escpos-commands-js/blob/master/src/commands.js
export var commands = {
    LF: "\x0a",
    ESC: "\x1b",
    FS: "\x1c",
    GS: "\x1d",
    US: "\x1f",
    FF: "\x0c",
    DLE: "\x10",
    DC1: "\x11",
    DC4: "\x14",
    EOT: "\x04",
    NUL: "\x00",
    EOL: "\n",
    HORIZONTAL_LINE: {
        HR_58MM: "================================",
        HR2_58MM: "********************************"
    },
    FEED_CONTROL_SEQUENCES: {
        CTL_LF: "\x0a",
        CTL_FF: "\x0c",
        CTL_CR: "\x0d",
        CTL_HT: "\x09",
        CTL_VT: "\x0b" // Vertical tab
    },
    LINE_SPACING: {
        LS_DEFAULT: "\x1b\x32",
        LS_SET: "\x1b\x33"
    },
    HARDWARE: {
        HW_INIT: "\x1b\x40",
        HW_SELECT: "\x1b\x3d\x01",
        HW_RESET: "\x1b\x3f\x0a\x00" // Reset printer hardware
    },
    CASH_DRAWER: {
        CD_KICK_2: "\x1b\x70\x00",
        CD_KICK_5: "\x1b\x70\x01" // Sends a pulse to pin 5 []
    },
    MARGINS: {
        BOTTOM: "\x1b\x4f",
        LEFT: "\x1b\x6c",
        RIGHT: "\x1b\x51" // Fix right size
    },
    PAPER: {
        PAPER_FULL_CUT: "\x1d\x56\x00",
        PAPER_PART_CUT: "\x1d\x56\x01",
        PAPER_CUT_A: "\x1d\x56\x41",
        PAPER_CUT_B: "\x1d\x56\x42" // Partial cut paper
    },
    TEXT_FORMAT: {
        TXT_NORMAL: "\x1b\x21\x00",
        TXT_2HEIGHT: "\x1b\x21\x10",
        TXT_2WIDTH: "\x1b\x21\x20",
        TXT_4SQUARE: "\x1b\x21\x30",
        TXT_CUSTOM_SIZE: function (width, height) {
            // other sizes
            var widthDec = (width - 1) * 16;
            var heightDec = height - 1;
            var sizeDec = widthDec + heightDec;
            return "\x1d\x21" + String.fromCharCode(sizeDec);
        },
        TXT_HEIGHT: {
            1: "\x00",
            2: "\x01",
            3: "\x02",
            4: "\x03",
            5: "\x04",
            6: "\x05",
            7: "\x06",
            8: "\x07"
        },
        TXT_WIDTH: {
            1: "\x00",
            2: "\x10",
            3: "\x20",
            4: "\x30",
            5: "\x40",
            6: "\x50",
            7: "\x60",
            8: "\x70"
        },
        TXT_UNDERL_OFF: "\x1b\x2d\x00",
        TXT_UNDERL_ON: "\x1b\x2d\x01",
        TXT_UNDERL2_ON: "\x1b\x2d\x02",
        TXT_BOLD_OFF: "\x1b\x45\x00",
        TXT_BOLD_ON: "\x1b\x45\x01",
        TXT_ITALIC_OFF: "\x1b\x35",
        TXT_ITALIC_ON: "\x1b\x34",
        TXT_FONT_A: "\x1b\x4d\x00",
        TXT_FONT_B: "\x1b\x4d\x01",
        TXT_FONT_C: "\x1b\x4d\x02",
        TXT_ALIGN_LT: "\x1b\x61\x00",
        TXT_ALIGN_CT: "\x1b\x61\x01",
        TXT_ALIGN_RT: "\x1b\x61\x02" // Right justification
    }
};
//# sourceMappingURL=printer-commands.js.map