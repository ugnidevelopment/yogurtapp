import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { UserdataService } from '../../services/userdata/userdata.service';
import { Storage } from '@ionic/storage';
var MainPage = /** @class */ (function () {
    function MainPage(router, nav, userdata, storage) {
        this.router = router;
        this.nav = nav;
        this.userdata = userdata;
        this.storage = storage;
        this.storage.get('USER_DATA').then(function (data) {
            console.log(data);
        });
    }
    MainPage.prototype.ngOnInit = function () {
    };
    MainPage.prototype.goToProductList = function () {
        this.nav.navigateForward('/tabs/products');
    };
    MainPage.prototype.goToCreateOrder = function () {
        this.nav.navigateForward('/tabs/createorder');
    };
    MainPage.prototype.goToCustomerList = function () {
        this.nav.navigateForward('/tabs/customerlist');
    };
    MainPage = tslib_1.__decorate([
        Component({
            selector: 'app-main',
            templateUrl: './main.page.html',
            styleUrls: ['./main.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Router,
            NavController,
            UserdataService,
            Storage])
    ], MainPage);
    return MainPage;
}());
export { MainPage };
//# sourceMappingURL=main.page.js.map