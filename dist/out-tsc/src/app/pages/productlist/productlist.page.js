import * as tslib_1 from "tslib";
import { Component } from "@angular/core";
import { WpRestApiService } from "../../services/wp-rest-api/wp-rest-api.service";
import { NavController, ToastController } from "@ionic/angular";
import { UserdataService } from "../../services/userdata/userdata.service";
import { Router } from "@angular/router";
import { Storage } from "@ionic/storage";
import { OrderService } from "../../services/order/order.service";
import { AlertService } from "../../services/alert/alert.service";
import { LoadingService } from "../../services/loading/loading.service";
var ProductlistPage = /** @class */ (function () {
    function ProductlistPage(wpRestApi, nav, userData, router, storage, order, toastCtrl, alert, loading) {
        this.wpRestApi = wpRestApi;
        this.nav = nav;
        this.userData = userData;
        this.router = router;
        this.storage = storage;
        this.order = order;
        this.toastCtrl = toastCtrl;
        this.alert = alert;
        this.loading = loading;
        this.loading.present("Cargando productos...");
        this.getProductList();
    }
    ProductlistPage.prototype.ngOnInit = function () { };
    ProductlistPage.prototype.doRefresh = function (event) {
        var _this = this;
        this.storage.get("TOKEN").then(function (token) {
            _this.wpRestApi
                .getWoocommerceProductList(token.token)
                .then(function (data) {
                _this.productList = data;
                _this.productList.forEach(function (element) {
                    element.totalCount = 0;
                    if (element.stock_quantity < 1) {
                        element.count = 0;
                    }
                    else {
                        element.count = 1;
                    }
                });
                event.target.complete();
            })
                .catch(function (err) {
                console.log(err);
            });
        });
    };
    ProductlistPage.prototype.showToast = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: "Producto cargado correctamente",
                            duration: 1500,
                            showCloseButton: true,
                            closeButtonText: "Ok"
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    ProductlistPage.prototype.getProductList = function () {
        var _this = this;
        this.storage.get("TOKEN").then(function (token) {
            _this.wpRestApi
                .getWoocommerceProductList(token.token)
                .then(function (data) {
                _this.productList = data;
                _this.productList.forEach(function (element) {
                    element.totalCount = 0;
                    if (element.stock_quantity < 1) {
                        element.count = 0;
                    }
                    else {
                        element.count = 1;
                    }
                });
                _this.loading.dismiss();
            })
                .catch(function (err) {
                console.log(err);
            });
        });
    };
    ProductlistPage.prototype.setProductToOrder = function (product) {
        if (product.count > product.stock_quantity) {
            this.alert.present("La cantidad supera la disponibilidad en stock.");
            return;
        }
        if (product.stock_quantity == 0) {
            this.alert.present("No hay stock.");
            return;
        }
        if (this.orderHaveProduct(product)) {
            this.increaseProduct(product);
        }
        else {
            product.totalCount = product.count;
            product.stock_quantity = product.stock_quantity - product.count;
            this.order.setProductToList(product);
            this.showToast();
        }
    };
    ProductlistPage.prototype.orderHaveProduct = function (product) {
        var haveProduct = false;
        this.order.getProductList().forEach(function (p) {
            if (p.id == product.id) {
                haveProduct = true;
            }
        });
        return haveProduct;
    };
    ProductlistPage.prototype.increaseProduct = function (product) {
        this.order.getProductList().forEach(function (p) {
            if (p.id == product.id) {
                p.totalCount += product.count;
                p.stock_quantity -= product.count;
                p.count = 1;
            }
        });
        this.showToast();
    };
    ProductlistPage.prototype.increment = function (product) {
        if (product.count == product.stock_quantity) {
            return;
        }
        product.count++;
    };
    ProductlistPage.prototype.decrement = function (product) {
        if (product.count == 0) {
            return;
        }
        product.count--;
    };
    ProductlistPage.prototype.gotToCreateOrder = function () {
        this.nav.navigateForward("createorder");
    };
    ProductlistPage = tslib_1.__decorate([
        Component({
            selector: "app-productlist",
            templateUrl: "./productlist.page.html",
            styleUrls: ["./productlist.page.scss"]
        }),
        tslib_1.__metadata("design:paramtypes", [WpRestApiService,
            NavController,
            UserdataService,
            Router,
            Storage,
            OrderService,
            ToastController,
            AlertService,
            LoadingService])
    ], ProductlistPage);
    return ProductlistPage;
}());
export { ProductlistPage };
//# sourceMappingURL=productlist.page.js.map