import * as tslib_1 from "tslib";
import { Component } from "@angular/core";
import { OrderService } from "../../services/order/order.service";
import { NavController, LoadingController, AlertController, ToastController } from "@ionic/angular";
import { Storage } from "@ionic/storage";
import { PrinterProvider } from "src/app/providers/printer/printer";
import { commands } from "./../../providers/printer/printer-commands";
import { WpRestApiService } from "../../services/wp-rest-api/wp-rest-api.service";
var CreateorderPage = /** @class */ (function () {
    function CreateorderPage(order, wpRestApi, nav, printer, alertCtrl, loadCtrl, toastCtrl, storage) {
        this.order = order;
        this.wpRestApi = wpRestApi;
        this.nav = nav;
        this.printer = printer;
        this.alertCtrl = alertCtrl;
        this.loadCtrl = loadCtrl;
        this.toastCtrl = toastCtrl;
        this.storage = storage;
        this.inputData = {};
        this.myPrinters = [];
        this.reteica = false;
        this.retefuente = false;
        this.reteicaVal = 4.14 / 1000;
        this.retefuenteVal = 2.5 / 100;
        this.ivaVal = 19 / 100;
        this.ivaTotal = 0;
        this.reteicaTotal = 0;
        this.retefuenteTotal = 0;
        this.subtotal = 0;
        this.couponDiscount = 0;
    }
    CreateorderPage.prototype.haveCustomer = function () {
        if (this.order.getCustomerData() !== undefined) {
            this.checkTaxes();
            return true;
        }
        return false;
    };
    CreateorderPage.prototype.checkRf = function (item) {
        return item.key == "retefuente";
    };
    CreateorderPage.prototype.checkRi = function (item) {
        return item.key == "reteica";
    };
    CreateorderPage.prototype.checkCoupon = function (item) {
        return item.key == "cupon";
    };
    CreateorderPage.prototype.checkTaxes = function () {
        if (this.order.getCustomerData().meta_data.find(this.checkRf) != undefined) {
            if (this.order.getCustomerData().meta_data.find(this.checkRf).value ===
                "yes") {
                this.retefuente = true;
            }
        }
        if (this.order.getCustomerData().meta_data.find(this.checkRi) != undefined) {
            if (this.order.getCustomerData().meta_data.find(this.checkRi).value ===
                "yes") {
                this.reteica = true;
            }
        }
    };
    CreateorderPage.prototype.goToCustomerList = function () {
        this.nav.navigateForward("/tabs/customerlist");
    };
    CreateorderPage.prototype.calculate = function () {
        var _this = this;
        this.reteica = false;
        this.retefuente = false;
        this.retefuenteTotal = 0;
        this.ivaTotal = 0;
        this.reteicaTotal = 0;
        this.subtotal = 0;
        this.couponDiscount = 0;
        var totalLine = 0;
        if (this.haveCustomer()) {
            var discPromise = new Promise(function (resolve, reject) {
                _this.storage.get("TOKEN").then(function (token) {
                    _this.wpRestApi
                        .getMembership(token.token)
                        .then(function (data) {
                        if (_this.order.getCustomerData().meta_data.find(_this.checkCoupon) !=
                            undefined) {
                            var _userCoupon_1 = _this.order
                                .getCustomerData()
                                .meta_data.find(_this.checkCoupon).value;
                            var _coupons = data.filter(function (coupon) {
                                if (_userCoupon_1.toUpperCase() == coupon.code.toUpperCase()) {
                                    return coupon;
                                }
                            });
                            if (_coupons.length != 0) {
                                _this.couponDiscount = _coupons[0].amount;
                            }
                            resolve(_this.couponDiscount);
                        }
                    })
                        .catch(function (err) {
                        console.log(err);
                    });
                });
            });
            discPromise.then(function (resolve) {
                var discount = 1 - resolve / 100;
                _this.order.getProductList().forEach(function (product) {
                    if (product.tax_status == "none") {
                        totalLine = product.totalCount * product.regular_price;
                    }
                    else {
                        totalLine = (product.totalCount * product.regular_price) / 1.19;
                        _this.ivaTotal += Math.ceil(totalLine * _this.ivaVal);
                    }
                    if (_this.retefuente) {
                        _this.retefuenteTotal += Math.ceil(totalLine * _this.retefuenteVal);
                    }
                    if (_this.reteica) {
                        _this.reteicaTotal += Math.ceil(totalLine * _this.reteicaVal);
                    }
                    /* console.log("Total line", totalLine);
                    console.log("IVA", this.ivaVal, this.ivaTotal);
                    console.log("Reteica", this.reteicaVal, this.reteicaTotal);
                    console.log("Retefuente", this.retefuenteVal, this.retefuenteTotal); */
                });
                _this.total = Math.ceil(_this.order.getOrderTotal() - _this.retefuenteTotal - _this.reteicaTotal);
                _this.subtotal = Math.ceil(totalLine);
                _this.total = Math.ceil(_this.total * discount);
                /* console.log("Total", this.total);
                console.log("Subtotal", this.subtotal); */
            });
        }
    };
    CreateorderPage.prototype.ionViewDidEnter = function () {
        console.log("entered page");
        this.calculate();
    };
    CreateorderPage.prototype.ngOnInit = function () { };
    CreateorderPage.prototype.goToProductList = function () {
        this.nav.navigateForward("/tabs/products");
    };
    CreateorderPage.prototype.createOrder = function () {
        var _this = this;
        this.order.createOrder().then(function (data) {
            console.log(data);
            _this.prepareToPrint();
        });
    };
    CreateorderPage.prototype.noSpecialChars = function (string) {
        var translate = {
            à: "a",
            á: "a",
            â: "a",
            ã: "a",
            ä: "a",
            å: "a",
            æ: "a",
            ç: "c",
            è: "e",
            é: "e",
            ê: "e",
            ë: "e",
            ì: "i",
            í: "i",
            î: "i",
            ï: "i",
            ð: "d",
            ñ: "n",
            ò: "o",
            ó: "o",
            ô: "o",
            õ: "o",
            ö: "o",
            ø: "o",
            ù: "u",
            ú: "u",
            û: "u",
            ü: "u",
            ý: "y",
            þ: "b",
            ÿ: "y",
            ŕ: "r",
            À: "A",
            Á: "A",
            Â: "A",
            Ã: "A",
            Ä: "A",
            Å: "A",
            Æ: "A",
            Ç: "C",
            È: "E",
            É: "E",
            Ê: "E",
            Ë: "E",
            Ì: "I",
            Í: "I",
            Î: "I",
            Ï: "I",
            Ð: "D",
            Ñ: "N",
            Ò: "O",
            Ó: "O",
            Ô: "O",
            Õ: "O",
            Ö: "O",
            Ø: "O",
            Ù: "U",
            Ú: "U",
            Û: "U",
            Ü: "U",
            Ý: "Y",
            Þ: "B",
            Ÿ: "Y",
            Ŕ: "R"
        }, translate_re = /[àáâãäåæçèéêëìíîïðñòóôõöøùúûüýþßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŕŕÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÝÝÞŸŔŔ]/gim;
        return string.replace(translate_re, function (match) {
            return translate[match];
        });
    };
    CreateorderPage.prototype.removeCustom = function (id) {
        this.order.removeProduct(id);
        this.calculate();
    };
    CreateorderPage.prototype.print = function (device, data) {
        var _this = this;
        console.log("Device mac: ", device);
        console.log("Data: ", data);
        this.loadCtrl
            .create({
            message: "Imprimiendo..."
        })
            .then(function (load) {
            load.present();
            _this.printer.connectBluetooth(device).subscribe(function (status) {
                console.log(status);
                _this.printer
                    .printData(_this.noSpecialChars(data))
                    .then(function (printStatus) {
                    console.log(printStatus);
                    load.dismiss();
                    _this.alertCtrl
                        .create({
                        message: "Impreso correctamente!",
                        buttons: [
                            {
                                text: "Ok",
                                handler: function () {
                                    _this.printer.disconnectBluetooth();
                                }
                            }
                        ]
                    })
                        .then(function (alert) {
                        alert.present();
                    });
                })
                    .catch(function (error) {
                    console.log(error);
                    load.dismiss();
                    _this.alertCtrl
                        .create({
                        message: "Hubo un error imprimiendo, porfavor inténtelo de nuevo!",
                        buttons: [
                            {
                                text: "Ok",
                                handler: function () {
                                    load.dismiss();
                                    _this.printer.disconnectBluetooth();
                                }
                            }
                        ]
                    })
                        .then(function (alert) {
                        alert.present();
                    });
                });
            }, function (error) {
                console.log(error);
                load.dismiss();
                var alert = _this.alertCtrl
                    .create({
                    message: "Hubo un error conectándose a la impresora, porfavor inténtelo de nuevo!",
                    buttons: [
                        {
                            text: "Ok",
                            handler: function () {
                                load.dismiss();
                                // this.printer.disconnectBluetooth();
                            }
                        }
                    ]
                })
                    .then(function (alert) {
                    alert.present();
                });
            });
        });
    };
    CreateorderPage.prototype.prepareToPrint = function () {
        // let total = 0;
        // u can remove this when generate the receipt using another method
        var data = { title: "", text: "", email: "" };
        data.title = "Lácteos El Portillo LTDA";
        data.email = "el.portillo@hotmail.com";
        data.text = "NIT: 900364558-4";
        var _date = new Date();
        var receipt = "";
        receipt += commands.HARDWARE.HW_INIT;
        receipt += commands.TEXT_FORMAT.TXT_2WIDTH;
        receipt += commands.TEXT_FORMAT.TXT_ALIGN_CT;
        receipt += data.title.toUpperCase();
        receipt += commands.EOL;
        receipt += commands.TEXT_FORMAT.TXT_NORMAL;
        receipt += data.email;
        receipt += commands.EOL;
        receipt += data.text;
        receipt += commands.EOL;
        receipt += commands.HORIZONTAL_LINE.HR_58MM;
        receipt += commands.EOL;
        receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
        receipt +=
            "Fecha: " +
                _date.toLocaleTimeString() +
                " - " +
                _date.toLocaleDateString();
        receipt += commands.EOL;
        receipt += "Cliente: " + this.order.getCustomerData().first_name;
        receipt += commands.EOL;
        receipt += "CC/NIT: " + this.order.getCustomerData().meta_data[0].value;
        receipt += commands.EOL;
        receipt += commands.HORIZONTAL_LINE.HR2_58MM;
        receipt += commands.EOL;
        receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
        receipt += "Producto  Costo U.  Cant.  Total";
        this.order.getProductList().forEach(function (product) {
            receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
            receipt += product.name;
            receipt += commands.EOL;
            receipt += commands.TEXT_FORMAT.TXT_ALIGN_RT;
            receipt += "$" + (product.regular_price * 1).toLocaleString("es-CO");
            receipt += "   ";
            receipt += (product.totalCount * 1).toLocaleString("es-CO");
            receipt += "  ";
            receipt +=
                "$" +
                    (product.totalCount * product.regular_price).toLocaleString("es-CO");
            receipt += commands.EOL;
        });
        if (this.reteica) {
            receipt += "Reteica $" + this.reteicaTotal;
            receipt += commands.EOL;
        }
        if (this.retefuente) {
            receipt += "Retefuente $" + this.retefuenteTotal;
            receipt += commands.EOL;
        }
        receipt += "IVA $" + this.ivaTotal;
        receipt += commands.EOL;
        receipt += "Descuentos %" + this.couponDiscount;
        receipt += commands.EOL;
        receipt += "Subtotal $" + this.subtotal;
        receipt += commands.EOL;
        receipt += "Total $" + this.total;
        receipt += commands.EOL;
        receipt += commands.TEXT_FORMAT.TXT_ALIGN_CT;
        receipt += "Transversal 80 B. 65 F 83 sur.  Tel 7750439. -. 320 3431841";
        // secure space on footer
        receipt += commands.EOL;
        receipt += commands.EOL;
        receipt += commands.EOL;
        // this.receipt = receipt;
        // console.log(receipt);
        this.mountAlertBt(receipt);
    };
    CreateorderPage.prototype.getType = function (val) {
        return typeof val;
    };
    CreateorderPage.prototype.showToast = function (data) {
        var toast = this.toastCtrl
            .create({
            duration: 3000,
            message: data,
            position: "bottom"
        })
            .then(function (toast) { return toast.present(); });
    };
    CreateorderPage.prototype.mountAlertBt = function (data) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.receipt = data;
                        return [4 /*yield*/, this.printer
                                .enableBluetooth()
                                .then(function () {
                                _this.printer
                                    .searchBluetooth()
                                    .then(function (devices) {
                                    devices.forEach(function (device) {
                                        console.log("Devices: ", JSON.stringify(device));
                                        _this.myPrinters.push({
                                            name: "printer",
                                            value: device.address,
                                            label: device.name,
                                            type: "radio"
                                        });
                                    });
                                })
                                    .catch(function (error) {
                                    console.log(error);
                                    _this.showToast("Hubo un error conectando la impresora, inténtelo de nuevo!");
                                    _this.mountAlertBt(_this.receipt);
                                });
                            })
                                .catch(function (error) {
                                console.log(error);
                                _this.showToast("Error activando bluetooth, inténtelo de nuevo!");
                                _this.mountAlertBt(_this.receipt);
                            })];
                    case 1:
                        _a.sent();
                        return [4 /*yield*/, this.alertCtrl.create({
                                message: "Seleccione la impresora",
                                inputs: this.myPrinters,
                                buttons: [
                                    {
                                        text: "Cancelar",
                                        role: "cancel"
                                    },
                                    {
                                        text: "Seleccione",
                                        handler: function (device) {
                                            if (!device) {
                                                _this.showToast("Seleccione una!");
                                                return false;
                                            }
                                            console.log(device);
                                            _this.print(device, _this.receipt);
                                        }
                                    }
                                ]
                            })];
                    case 2:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 3:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    CreateorderPage = tslib_1.__decorate([
        Component({
            selector: "app-createorder",
            templateUrl: "./createorder.page.html",
            styleUrls: ["./createorder.page.scss"]
        }),
        tslib_1.__metadata("design:paramtypes", [OrderService,
            WpRestApiService,
            NavController,
            PrinterProvider,
            AlertController,
            LoadingController,
            ToastController,
            Storage])
    ], CreateorderPage);
    return CreateorderPage;
}());
export { CreateorderPage };
//# sourceMappingURL=createorder.page.js.map