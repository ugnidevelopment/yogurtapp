import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CreateorderPage } from './createorder.page';
var routes = [
    {
        path: '',
        component: CreateorderPage
    }
];
var CreateorderPageModule = /** @class */ (function () {
    function CreateorderPageModule() {
    }
    CreateorderPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [CreateorderPage]
        })
    ], CreateorderPageModule);
    return CreateorderPageModule;
}());
export { CreateorderPageModule };
//# sourceMappingURL=createorder.module.js.map