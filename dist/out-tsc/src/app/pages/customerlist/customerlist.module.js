import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CustomerlistPage } from './customerlist.page';
var routes = [
    {
        path: '',
        component: CustomerlistPage
    }
];
var CustomerlistPageModule = /** @class */ (function () {
    function CustomerlistPageModule() {
    }
    CustomerlistPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [CustomerlistPage]
        })
    ], CustomerlistPageModule);
    return CustomerlistPageModule;
}());
export { CustomerlistPageModule };
//# sourceMappingURL=customerlist.module.js.map