import * as tslib_1 from "tslib";
import { Component } from "@angular/core";
import { WpRestApiService } from "../../services/wp-rest-api/wp-rest-api.service";
import { NavController } from "@ionic/angular";
import { UserdataService } from "../../services/userdata/userdata.service";
import { Router } from "@angular/router";
import { OrderService } from "../../services/order/order.service";
import { Storage } from "@ionic/storage";
import { LoadingService } from "../../services/loading/loading.service";
var CustomerlistPage = /** @class */ (function () {
    function CustomerlistPage(wpRestApi, nav, userData, router, order, storage, loading) {
        this.wpRestApi = wpRestApi;
        this.nav = nav;
        this.userData = userData;
        this.router = router;
        this.order = order;
        this.storage = storage;
        this.loading = loading;
        this.showList = false;
        this.loading.present("Cargando clientes...");
        this.getCustomers();
    }
    CustomerlistPage.prototype.ngOnInit = function () { };
    CustomerlistPage.prototype.getCustomers = function () {
        var _this = this;
        this.storage.get("TOKEN").then(function (token) {
            _this.wpRestApi
                .getWordpressUserByRole("cliente", token.token)
                .then(function (data) {
                _this.customerList = data;
                _this.makeDocument();
                _this.showList = true;
                _this.loading.dismiss();
            })
                .catch(function (err) {
                console.log(err);
            });
        });
    };
    CustomerlistPage.prototype.checkCC = function (item) {
        return item.key == "cedula";
    };
    CustomerlistPage.prototype.makeDocument = function () {
        var _this = this;
        this.customerList.forEach(function (customer) {
            if (customer.meta_data.find(_this.checkCC) != undefined) {
                customer.cc = customer.meta_data.find(_this.checkCC).value;
            }
        });
    };
    CustomerlistPage.prototype.setCustomerToOrder = function (customer) {
        this.order.setCustomerData(customer);
        this.goToCreateOrder();
        console.log(this.order.getCustomerData());
    };
    CustomerlistPage.prototype.goToCreateOrder = function () {
        this.nav.navigateForward("/tabs/createorder");
    };
    CustomerlistPage.prototype.doRefresh = function (event) {
        var _this = this;
        this.storage.get("TOKEN").then(function (token) {
            _this.wpRestApi
                .getWordpressUserByRole("cliente", token.token)
                .then(function (data) {
                _this.customerList = data;
                _this.showList = true;
                event.target.complete();
            })
                .catch(function (err) {
                console.log(err);
            });
        });
    };
    CustomerlistPage = tslib_1.__decorate([
        Component({
            selector: "app-customerlist",
            templateUrl: "./customerlist.page.html",
            styleUrls: ["./customerlist.page.scss"]
        }),
        tslib_1.__metadata("design:paramtypes", [WpRestApiService,
            NavController,
            UserdataService,
            Router,
            OrderService,
            Storage,
            LoadingService])
    ], CustomerlistPage);
    return CustomerlistPage;
}());
export { CustomerlistPage };
//# sourceMappingURL=customerlist.page.js.map