import * as tslib_1 from "tslib";
import { Component } from "@angular/core";
import { NavController } from "@ionic/angular";
import { Router } from "@angular/router";
import { LoadingService } from "../../services/loading/loading.service";
import { WpRestApiService } from "../../services/wp-rest-api/wp-rest-api.service";
import { UserdataService } from "../../services/userdata/userdata.service";
import { Storage } from "@ionic/storage";
import { AlertService } from "../../services/alert/alert.service";
var WelcomePage = /** @class */ (function () {
    function WelcomePage(loading, wpRestApi, userData, nav, router, storage, alert) {
        this.loading = loading;
        this.wpRestApi = wpRestApi;
        this.userData = userData;
        this.nav = nav;
        this.router = router;
        this.storage = storage;
        this.alert = alert;
        this.userName = "ovidio";
        this.password = "appelportillo2020+";
        this.showErrorMessage = false;
        this.loginErrorMessage = false;
        this.showLogin = false;
        this.verifyAccess();
    }
    WelcomePage.prototype.verifyAccess = function () {
        var _this = this;
        this.storage.remove("TOKEN");
        this.storage.get("TOKEN").then(function (token) {
            if (token !== null) {
                _this.userData.setToken(token);
                _this.userData.setUserData().then(function () {
                    _this.goToMainPage();
                });
            }
            else {
                _this.showLogin = true;
            }
        });
    };
    WelcomePage.prototype.ngOnInit = function () { };
    WelcomePage.prototype.login = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                if (!this.fieldsEmpty()) {
                    this.showErrorMessage = false;
                    this.loading.present("Entrando al sistema...");
                    this.getJWTToken({ username: this.userName, password: this.password });
                }
                else {
                    this.showErrorMessage = true;
                }
                return [2 /*return*/];
            });
        });
    };
    WelcomePage.prototype.fieldsEmpty = function () {
        if (this.userName == "" || this.password == "")
            return true;
        return false;
    };
    WelcomePage.prototype.getJWTToken = function (data) {
        var _this = this;
        this.wpRestApi
            .getJWTToken(data)
            .then(function (res) {
            console.log(res);
            _this.token = res;
            if (_this.token.token === undefined) {
                _this.loading.dismiss();
                _this.loginErrorMessage = true;
            }
            else {
                _this.loading.dismiss();
                _this.loginErrorMessage = false;
                _this.userData.setToken(_this.token);
                _this.userData.setUserData();
                _this.goToMainPage();
            }
        })
            .catch(function (err) {
            console.log(err);
        });
    };
    WelcomePage.prototype.goToMainPage = function () {
        this.nav.navigateRoot("tabs");
    };
    WelcomePage.prototype.recoveryPassword = function () {
        this.alert.recoveryPassword();
    };
    WelcomePage = tslib_1.__decorate([
        Component({
            selector: "app-welcome",
            templateUrl: "./welcome.page.html",
            styleUrls: ["./welcome.page.scss"]
        }),
        tslib_1.__metadata("design:paramtypes", [LoadingService,
            WpRestApiService,
            UserdataService,
            NavController,
            Router,
            Storage,
            AlertService])
    ], WelcomePage);
    return WelcomePage;
}());
export { WelcomePage };
//# sourceMappingURL=welcome.page.js.map