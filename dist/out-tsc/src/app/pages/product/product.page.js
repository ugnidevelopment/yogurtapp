import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { WpRestApiService } from '../../services/wp-rest-api/wp-rest-api.service';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { UserdataService } from '../../services/userdata/userdata.service';
import { OrderService } from '../../services/order/order.service';
import { AlertService } from '../../services/alert/alert.service';
var ProductPage = /** @class */ (function () {
    function ProductPage(wpRestApi, nav, route, userData, order, alert) {
        this.wpRestApi = wpRestApi;
        this.nav = nav;
        this.route = route;
        this.userData = userData;
        this.order = order;
        this.alert = alert;
        this.showData = false;
        this.count = 1;
    }
    ProductPage.prototype.ngOnInit = function () {
        this.id = this.route.snapshot.paramMap.get('id');
        this.getProduct();
    };
    ProductPage.prototype.getProduct = function () {
        var _this = this;
        this.wpRestApi.getWoocommerceProduct(this.id, this.userData.getTokenCode())
            .then(function (data) {
            _this.data = data;
            _this.showData = true;
        })
            .catch(function (err) {
            console.log(err);
        });
    };
    ProductPage.prototype.setCountProduct = function (product) {
        product.count = this.count;
        return product;
    };
    ProductPage.prototype.setProdutToOrder = function () {
        if (this.orderHaveProduct()) {
            console.log('el producto ya existe');
            this.alert.present('El producto ya existe en la orden.');
            return;
        }
        console.log(this.orderHaveProduct());
        this.order.setProductToList(this.setCountProduct(this.data));
        this.gotToCreateOrder();
    };
    ProductPage.prototype.gotToCreateOrder = function () {
        this.nav.navigateForward('createorder');
    };
    ProductPage.prototype.orderHaveProduct = function () {
        var _this = this;
        console.log('me llamaron');
        var haveProduct = false;
        this.order.getProductList().forEach(function (product) {
            if (product.id == _this.data.id)
                haveProduct = true;
        });
        return haveProduct;
    };
    ProductPage = tslib_1.__decorate([
        Component({
            selector: 'app-product',
            templateUrl: './product.page.html',
            styleUrls: ['./product.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [WpRestApiService,
            NavController,
            ActivatedRoute,
            UserdataService,
            OrderService,
            AlertService])
    ], ProductPage);
    return ProductPage;
}());
export { ProductPage };
//# sourceMappingURL=product.page.js.map