import * as tslib_1 from "tslib";
import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
var WpRestApiService = /** @class */ (function () {
    function WpRestApiService(http) {
        this.http = http;
        this.apiUrl = "https://applacteoselportillo.com/wp-json/";
        this.consumerKey = "ck_3ad9dd354bf4b0a8efbbbf47633304e64eba93df";
        this.consumerSecret = "cs_aabb08d233b82b6abbdc108bef4944b75356f946";
    }
    WpRestApiService.prototype.getJWTToken = function (data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.apiUrl + "jwt-auth/v1/token", data).subscribe(function (res) {
                resolve(res);
            }, function (err) {
                resolve(err);
            });
        });
    };
    WpRestApiService.prototype.getWordpressUserData = function (userId, token) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http
                .get(_this.apiUrl + "wp/v2/users/" + userId, {
                headers: new HttpHeaders().set("Authorization", "Bearer " + token)
            })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    WpRestApiService.prototype.getWoocommerceProductList = function (token) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http
                .get(_this.apiUrl +
                "wc/v3/products?per_page=100&consumer_key=" +
                _this.consumerKey +
                "&consumer_secret=" +
                _this.consumerSecret, {
                headers: new HttpHeaders().set("Authorization", "Bearer " + token)
            })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    WpRestApiService.prototype.getWoocommerceProduct = function (id, token) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http
                .get(_this.apiUrl +
                "wc/v3/products/" +
                id +
                "?consumer_key=" +
                _this.consumerKey +
                "&consumer_secret=" +
                _this.consumerSecret, {
                headers: new HttpHeaders().set("Authorization", "Bearer " + token)
            })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    WpRestApiService.prototype.setWoocommerceOrder = function (data, token) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http
                .post(_this.apiUrl +
                "wc/v3/orders?consumer_key=" +
                _this.consumerKey +
                "&consumer_secret=" +
                _this.consumerSecret, data, {
                headers: new HttpHeaders().set("Authorization", "Bearer " + token)
            })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    WpRestApiService.prototype.getWordpressUserByRole = function (role, token) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http
                .get(_this.apiUrl +
                "wc/v3/customers" +
                "?consumer_key=" +
                _this.consumerKey +
                "&consumer_secret=" +
                _this.consumerSecret, {
                headers: new HttpHeaders().set("Authorization", "Bearer " + token)
            })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    WpRestApiService.prototype.getMembership = function (token) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http
                .get(_this.apiUrl +
                "wc/v3/coupons" +
                "?consumer_key=" +
                _this.consumerKey +
                "&consumer_secret=" +
                _this.consumerSecret, {
                headers: new HttpHeaders().set("Authorization", "Bearer " + token)
            })
                .subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    WpRestApiService = tslib_1.__decorate([
        Injectable({
            providedIn: "root"
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], WpRestApiService);
    return WpRestApiService;
}());
export { WpRestApiService };
//# sourceMappingURL=wp-rest-api.service.js.map