import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { UserdataService } from '../userdata/userdata.service';
var AlertService = /** @class */ (function () {
    function AlertService(alert, userdata) {
        this.alert = alert;
        this.userdata = userdata;
    }
    AlertService.prototype.present = function (message) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alert.create({
                            header: '',
                            message: message,
                            buttons: ['OK']
                        }).then(function (a) {
                            a.present();
                        })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    AlertService.prototype.recoveryPassword = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alert.create({
                            header: 'Recuperar contraseña',
                            message: 'Introduce tu nombre de usuario',
                            buttons: [{
                                    text: 'OK',
                                    handler: function (data) {
                                        if (data.username.length > 0) {
                                            _this.userdata.recoveryPassword(data.username);
                                        }
                                        else {
                                            _this.present('Introduce un nombre de usuario valido.');
                                        }
                                    }
                                }],
                            inputs: [{
                                    name: 'username'
                                }],
                        }).then(function (a) {
                            a.present().then(function (data) { console.log(data); });
                        })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    AlertService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [AlertController,
            UserdataService])
    ], AlertService);
    return AlertService;
}());
export { AlertService };
//# sourceMappingURL=alert.service.js.map