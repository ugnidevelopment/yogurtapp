import * as tslib_1 from "tslib";
import { Injectable } from "@angular/core";
import { WpRestApiService } from "../wp-rest-api/wp-rest-api.service";
import { UserdataService } from "../userdata/userdata.service";
import { Storage } from "@ionic/storage";
var OrderService = /** @class */ (function () {
    function OrderService(wpRest, userData, storage) {
        this.wpRest = wpRest;
        this.userData = userData;
        this.storage = storage;
        this.orderTotal = 0;
        this.orderUnitTotal = 0;
        this.productList = [];
    }
    OrderService.prototype.calculateOrderTotal = function () {
        var _this = this;
        this.orderTotal = 0;
        this.productList.forEach(function (product) {
            _this.orderTotal += product.count * product.regular_price;
        });
    };
    OrderService.prototype.getOrderTotal = function () {
        return this.orderTotal;
    };
    OrderService.prototype.calculateOrderUnitTotal = function () {
        var _this = this;
        this.orderUnitTotal = 0;
        this.productList.forEach(function (product) {
            _this.orderUnitTotal += product.count;
        });
    };
    OrderService.prototype.getOrderUnitTotal = function () {
        return this.orderUnitTotal;
    };
    OrderService.prototype.setCustomerData = function (customerData) {
        this.customerData = customerData;
    };
    OrderService.prototype.getCustomerData = function () {
        return this.customerData;
    };
    OrderService.prototype.setProductToList = function (product) {
        this.productList.push(product);
        this.calculateOrderTotal();
        this.calculateOrderUnitTotal();
    };
    OrderService.prototype.getProductList = function () {
        return this.productList;
    };
    OrderService.prototype.removeProduct = function (id) {
        var _this = this;
        this.productList.forEach(function (el, i) {
            if (el.id == id)
                _this.productList.splice(i, 1);
        });
        this.calculateOrderTotal();
        this.calculateOrderUnitTotal();
    };
    OrderService.prototype.createOrder = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var ud, orderData;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.storage.get("USER_DATA")];
                    case 1:
                        ud = _a.sent();
                        orderData = {
                            set_paid: true,
                            billing: {
                                first_name: this.customerData.billing.first_name,
                                last_name: this.customerData.billing.last_name,
                                address_1: this.customerData.billing.address_1,
                                address_2: this.customerData.billing.adders_2,
                                city: this.customerData.billing.city,
                                state: this.customerData.billing.state,
                                postcode: this.customerData.billing.postcode,
                                country: this.customerData.billing.country,
                                email: this.customerData.billing.email,
                                phone: this.customerData.billing.phone
                            },
                            shipping: {
                                first_name: ud.firstname,
                                last_name: ud.lastname,
                                address_1: "Id: " + ud.id,
                                address_2: "",
                                city: "Bogota",
                                state: "BOG",
                                postcode: "010101",
                                country: "CO"
                            },
                            line_items: []
                        };
                        this.productList.forEach(function (p) {
                            orderData.line_items.push({
                                product_id: p.id,
                                quantity: p.count
                            });
                        });
                        return [2 /*return*/, this.wpRest
                                .setWoocommerceOrder(orderData, this.userData.getTokenCode())
                                .then(function (result) {
                                return result;
                            })
                                .catch(function (err) {
                                console.log(err);
                            })];
                }
            });
        });
    };
    OrderService = tslib_1.__decorate([
        Injectable({
            providedIn: "root"
        }),
        tslib_1.__metadata("design:paramtypes", [WpRestApiService,
            UserdataService,
            Storage])
    ], OrderService);
    return OrderService;
}());
export { OrderService };
//# sourceMappingURL=order.service.js.map