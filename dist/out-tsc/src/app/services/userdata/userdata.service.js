import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { WpRestApiService } from '../wp-rest-api/wp-rest-api.service';
import { Storage } from '@ionic/storage';
var UserdataService = /** @class */ (function () {
    function UserdataService(wpRest, storage) {
        this.wpRest = wpRest;
        this.storage = storage;
    }
    UserdataService.prototype.getUserId = function () {
        return this.userId;
    };
    UserdataService.prototype.setUserId = function (id) {
        this.userId = id;
    };
    UserdataService.prototype.getTokenCode = function () {
        return this.tokenCode;
    };
    UserdataService.prototype.setTokenCode = function (tokenCode) {
        this.tokenCode = tokenCode;
    };
    UserdataService.prototype.setToken = function (token) {
        this.token = token;
        this.storage.set('TOKEN', this.token);
    };
    UserdataService.prototype.setUserData = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var JWT;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                this.setTokenCode(this.token.token);
                JWT = new JwtHelperService();
                this.setUserId(JWT.decodeToken(this.token.token).data.user.id);
                this.wpRest.getWordpressUserData(this.getUserId(), this.getTokenCode())
                    .then(function (data) {
                    _this.data = data;
                    _this.storage.set('USER_DATA', _this.data);
                })
                    .catch(function (err) {
                    console.log(err);
                });
                return [2 /*return*/];
            });
        });
    };
    UserdataService.prototype.getUserData = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                return [2 /*return*/, this.data];
            });
        });
    };
    UserdataService.prototype.recoveryPassword = function (username) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                console.log(username);
                return [2 /*return*/];
            });
        });
    };
    UserdataService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [WpRestApiService,
            Storage])
    ], UserdataService);
    return UserdataService;
}());
export { UserdataService };
//# sourceMappingURL=userdata.service.js.map