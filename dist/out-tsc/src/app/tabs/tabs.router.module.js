import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TabsPage } from './tabs.page';
var routes = [
    {
        path: '',
        component: TabsPage,
        children: [
            {
                path: 'main',
                children: [
                    {
                        path: '',
                        loadChildren: '../pages/main/main.module#MainPageModule'
                    }
                ]
            },
            {
                path: 'products',
                children: [
                    {
                        path: '',
                        loadChildren: '../pages/productlist/productlist.module#ProductlistPageModule'
                    }
                ]
            },
            {
                path: 'createorder',
                children: [
                    {
                        path: '',
                        loadChildren: '../pages/createorder/createorder.module#CreateorderPageModule'
                    }
                ]
            },
            {
                path: 'customerlist',
                children: [
                    {
                        path: '',
                        loadChildren: '../pages/customerlist/customerlist.module#CustomerlistPageModule'
                    }
                ]
            },
            {
                path: '',
                redirectTo: '/tabs/main',
                pathMatch: 'full'
            }
        ]
    }
];
var TabsPageRoutingModule = /** @class */ (function () {
    function TabsPageRoutingModule() {
    }
    TabsPageRoutingModule = tslib_1.__decorate([
        NgModule({
            imports: [
                RouterModule.forChild(routes)
            ],
            exports: [RouterModule]
        })
    ], TabsPageRoutingModule);
    return TabsPageRoutingModule;
}());
export { TabsPageRoutingModule };
//# sourceMappingURL=tabs.router.module.js.map