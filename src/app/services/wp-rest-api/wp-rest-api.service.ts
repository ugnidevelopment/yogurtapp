import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WpRestApiService {
  apiUrl = 'https://applacteoselportillo.com/wp-json/';
  consumerKey = 'ck_3ad9dd354bf4b0a8efbbbf47633304e64eba93df';
  consumerSecret = 'cs_aabb08d233b82b6abbdc108bef4944b75356f946';

  constructor(public http: HttpClient) {}

  getJWTToken(data) {
    return new Promise((resolve, reject) => {
      this.http.post(this.apiUrl + 'jwt-auth/v1/token', data).subscribe(
        res => {
          resolve(res);
        },
        err => {
          resolve(err);
        }
      );
    });
  }

  getWordpressUserData(userId: number, token: string) {
    return new Promise(resolve => {
      this.http
        .get(this.apiUrl + 'wp/v2/users/' + userId, {
          headers: new HttpHeaders().set('Authorization', 'Bearer ' + token)
        })
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            console.log(err);
          }
        );
    });
  }

  updateBilling(userId: number, token: string, current: number) {
    return new Promise(resolve => {
      this.http
        .post(this.apiUrl + 'yogurtapp/v1/userupdate/' + userId,
          { 'bill_current': current },
          { headers: new HttpHeaders().set('Authorization', 'Bearer ' + token)
        })
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            console.log(err);
          }
        );
    });
  }

  getWoocommerceProductList(token: string) {
    return new Promise(resolve => {
      this.http
        .get(
          this.apiUrl +
            'wc/v3/products?per_page=100&consumer_key=' +
            this.consumerKey +
            '&consumer_secret=' +
            this.consumerSecret,
          {
            headers: new HttpHeaders().set('Authorization', 'Bearer ' + token)
          }
        )
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            console.log(err);
          }
        );
    });
  }

  getWoocommerceProduct(id: string, token: string) {
    return new Promise(resolve => {
      this.http
        .get(
          this.apiUrl +
            'wc/v3/products/' +
            id +
            '?consumer_key=' +
            this.consumerKey +
            '&consumer_secret=' +
            this.consumerSecret,
          {
            headers: new HttpHeaders().set('Authorization', 'Bearer ' + token)
          }
        )
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            console.log(err);
          }
        );
    });
  }

  setWoocommerceOrder(data: any, token: string) {
    return new Promise(resolve => {
      this.http
        .post(
          this.apiUrl +
            'wc/v3/orders?consumer_key=' +
            this.consumerKey +
            '&consumer_secret=' +
            this.consumerSecret,
          data,
          {
            headers: new HttpHeaders().set('Authorization', 'Bearer ' + token)
          }
        )
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            console.log(err);
          }
        );
    });
  }

  getWordpressUserByRole(role: string, token: string) {
    return new Promise(resolve => {
      this.http
        .get(
          this.apiUrl +
            'wc/v3/customers?per_page=100' +
            '&consumer_key=' +
            this.consumerKey +
            '&consumer_secret=' +
            this.consumerSecret,
          {
            headers: new HttpHeaders().set('Authorization', 'Bearer ' + token)
          }
        )
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            console.log(err);
          }
        );
    });
  }

  getMembership(token: string) {
    return new Promise<any>(resolve => {
      this.http
        .get(
          this.apiUrl +
            'wc/v3/coupons' +
            '?consumer_key=' +
            this.consumerKey +
            '&consumer_secret=' +
            this.consumerSecret,
          {
            headers: new HttpHeaders().set('Authorization', 'Bearer ' + token)
          }
        )
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            console.log(err);
          }
        );
    });
  }
}
