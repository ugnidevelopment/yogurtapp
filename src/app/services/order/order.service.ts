import { Injectable } from '@angular/core';
import { WpRestApiService } from '../wp-rest-api/wp-rest-api.service';
import { UserdataService } from '../userdata/userdata.service';
import { Storage } from '@ionic/storage';
import { JwtHelperService } from '@auth0/angular-jwt';
@Injectable({
  providedIn: 'root',
})
export class OrderService {
  private customerData: any;
  private productList: any;
  private orderTotal = 0;
  private orderUnitTotal = 0;

  constructor(
    private wpRest: WpRestApiService,
    private userData: UserdataService,
    private storage: Storage
  ) {
    this.productList = [];
  }

  calculateOrderTotal() {
    this.orderTotal = 0;

    this.productList.forEach((product) => {
      this.orderTotal += product.count * product.regular_price;
    });
  }

  getOrderTotal() {
    return this.orderTotal;
  }

  calculateOrderUnitTotal() {
    this.orderUnitTotal = 0;
    this.productList.forEach((product) => {
      this.orderUnitTotal += product.count;
    });
  }

  getOrderUnitTotal() {
    return this.orderUnitTotal;
  }

  setCustomerData(customerData: any) {
    this.customerData = customerData;
  }

  getCustomerData() {
    return this.customerData;
  }

  setProductToList(product: any) {
    this.productList.push(product);
    this.calculateOrderTotal();
    this.calculateOrderUnitTotal();
  }

  getProductList() {
    return this.productList;
  }

  removeProduct(id: number) {
    this.productList.forEach((el, i) => {
      if (el.id == id) { this.productList.splice(i, 1); }
    });

    this.calculateOrderTotal();
    this.calculateOrderUnitTotal();
  }

  async createOrder(quote?: boolean, data?: string) {
    let quoteStr = '';
    let dataStr = '';
    if (quote) {
      quoteStr = 'Cotizacion ';
    }
    if (dataStr != null) {
      dataStr = data;
    }
    const ud = await this.storage.get('USER_DATA');
    const orderData: any = {
      set_paid: true,
      billing: {
        first_name: quoteStr + this.customerData.billing.first_name,
        last_name: this.customerData.billing.last_name,
        address_1: this.customerData.billing.address_1,
        address_2: this.customerData.billing.adders_2,
        city: this.customerData.billing.city,
        state: this.customerData.billing.state,
        postcode: this.customerData.billing.postcode,
        country: this.customerData.billing.country,
        email: this.customerData.billing.email,
        phone: this.customerData.billing.phone,
      },
      shipping: {
        first_name: 'Consecutivo ' + dataStr + ' - ' + ud.firstname,
        last_name: ud.lastname,
        address_1: 'Id: ' + ud.id,
        address_2: '',
        city: 'Bogota',
        state: 'BOG',
        postcode: '010101',
        country: 'CO',
      },
      line_items: [],
    };
    this.productList.forEach((p) => {
      orderData.line_items.push({
        product_id: p.id,
        quantity: p.count,
      });
    });

    this.storage.get('TOKEN').then((val) => {
      return this.wpRest
        .setWoocommerceOrder(orderData, val.token)
        .then((result) => {
          return result;
        })
        .catch((err) => {
          console.log(err);
        });
    });
  }
}
