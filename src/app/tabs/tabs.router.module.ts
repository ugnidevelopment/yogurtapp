import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TabsPage } from "./tabs.page";

const routes: Routes = [
  {
    path: "",
    component: TabsPage,
    children: [
      {
        path: "main",
        children: [
          {
            path: "",
            loadChildren: "../pages/main/main.module#MainPageModule",
          },
        ],
      },
      {
        path: "products",
        children: [
          {
            path: "",
            loadChildren:
              "../pages/productlist/productlist.module#ProductlistPageModule",
          },
        ],
      },
      {
        path: "createorder",
        children: [
          {
            path: "",
            loadChildren:
              "../pages/createorder/createorder.module#CreateorderPageModule",
          },
        ],
      },
      {
        path: "customerlist",
        children: [
          {
            path: "",
            loadChildren:
              "../pages/customerlist/customerlist.module#CustomerlistPageModule",
          },
        ],
      },
      {
        path: "quote",
        children: [
          {
            path: "",
            loadChildren: "../pages/quote/quote.module#QuotePageModule",
          },
        ],
      },
      {
        path: "quotecustomerlist",
        children: [
          {
            path: "",
            loadChildren:
              "../pages/quotecustomerlist/quotecustomerlist.module#QuoteCustomerlistPageModule",
          },
        ],
      },
      {
        path: "",
        redirectTo: "/tabs/main",
        pathMatch: "full",
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
