import { Component, OnInit } from '@angular/core';
import { OrderService } from '../../services/order/order.service';
import {
  NavController,
  LoadingController,
  AlertController,
  ToastController,
} from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { PrinterProvider } from 'src/app/providers/printer/printer';
import { commands } from './../../providers/printer/printer-commands';
import { WpRestApiService } from '../../services/wp-rest-api/wp-rest-api.service';
import { UserdataService } from "../../services/userdata/userdata.service";

@Component({
  selector: 'app-createorder',
  templateUrl: './createorder.page.html',
  styleUrls: ['./createorder.page.scss'],
})
export class CreateorderPage implements OnInit {
  receipt: any;
  inputData: any = {};
  myPrinters = [];
  stringVendor = '';
  reteica = false;
  retefuente = false;
  reteicaVal = 4.14 / 1000;
  retefuenteVal = 2.5 / 100;
  ivaVal = 19 / 100;
  ivaTotal = 0;
  reteicaTotal = 0;
  retefuenteTotal = 0;
  total: any;
  subtotal = 0;
  document = '';
  couponDiscount = 0;
  discountValue = 0;
  discPromise;
  constructor(
    public order: OrderService,
    public wpRestApi: WpRestApiService,
    public nav: NavController,
    private printer: PrinterProvider,
    private alertCtrl: AlertController,
    private loadCtrl: LoadingController,
    private toastCtrl: ToastController,
    public userData: UserdataService,
    private storage: Storage
  ) { }

  makeStringVendor() {
    this.userData.getUserData().then((data) => {
      let currentNumber;
      const _data = data as BillData;
      if (_data.bill_current === '') {
        // tslint:disable-next-line: radix
        currentNumber = parseInt(_data.bill_from) + 1;
      } else {
        // tslint:disable-next-line: radix
        currentNumber = parseInt(_data.bill_current) + 1;
      }
      this.userData.setBillingNumber(currentNumber);
      this.stringVendor = _data.bill_prefix + '-' + currentNumber + ' / del ' + _data.bill_from + ' al ' + _data.bill_to;
    }).catch((err) => {
      console.log(err);
    });
  }

  haveCustomer(): boolean {
    if (this.order.getCustomerData() !== undefined) {
      this.checkTaxes();
      return true;
    }
    return false;
  }

  checkRf(item) {
    return item.key === 'retefuente';
  }
  checkRi(item) {
    return item.key === 'reteica';
  }
  checkCoupon(item) {
    return item.key === 'cupon';
  }

  checkTaxes() {
    if (
      this.order.getCustomerData().meta_data.find(this.checkRf) != undefined
    ) {
      if (
        this.order.getCustomerData().meta_data.find(this.checkRf).value ===
        'yes'
      ) {
        this.retefuente = true;
      }
    }
    if (
      this.order.getCustomerData().meta_data.find(this.checkRi) != undefined
    ) {
      if (
        this.order.getCustomerData().meta_data.find(this.checkRi).value ===
        'yes'
      ) {
        this.reteica = true;
      }
    }
  }

  goToCustomerList() {
    this.nav.navigateForward('/tabs/customerlist');
  }

  checkDocument(item) {
    return item.key === 'cedula';
  }

  calculate() {
    this.reteica = false;
    this.retefuente = false;
    this.retefuenteTotal = 0;
    this.ivaTotal = 0;
    this.reteicaTotal = 0;
    this.subtotal = 0;
    this.couponDiscount = 0;
    this.discountValue = 0;
    let totalLine = 0;
    if (this.haveCustomer()) {
      const discPromise = new Promise<number>((resolve, reject) => {
        this.storage.get('TOKEN').then((token) => {
          this.wpRestApi
            .getMembership(token.token)
            .then((data) => {
              this.document = this.order
                .getCustomerData()
                .meta_data.find(this.checkDocument).value;
              if (
                this.order.getCustomerData().meta_data.find(this.checkCoupon) !=
                undefined
              ) {
                const _userCoupon = this.order
                  .getCustomerData()
                  .meta_data.find(this.checkCoupon).value;
                const _coupons = data.filter((coupon) => {
                  if (_userCoupon.toUpperCase() === coupon.code.toUpperCase()) {
                    return coupon;
                  }
                });
                if (_coupons.length !== 0) {
                  this.couponDiscount = _coupons[0].amount;
                }
                resolve(this.couponDiscount);
              }
            })
            .catch((err) => {
              console.log(err);
            });
        });
      });
      discPromise.then((resolve) => {
        this.order.getProductList().forEach((product) => {
          console.log(product);
          if (product.tax_status === 'none') {
            totalLine += product.totalCount * product.regular_price;
            console.log('subTotal', totalLine);
            this.discountValue += Math.floor(product.totalCount * product.regular_price * (resolve / 100));
            console.log('discount', this.discountValue);
          } else {
            totalLine += (product.totalCount * product.regular_price) / 1.19;
            console.log('subTotal', totalLine);
            this.discountValue += Math.floor((product.totalCount * product.regular_price / 1.19) * (resolve / 100));
            console.log('discount', this.discountValue);
            this.ivaTotal += Math.floor((product.totalCount * product.regular_price / 1.19) * (1 - resolve / 100) * this.ivaVal);
            console.log('iva', this.ivaTotal);
          }
        });
        this.subtotal = Math.floor(totalLine);
        console.log('subtotal Total ', this.subtotal);
        if (this.retefuente) {
          this.retefuenteTotal = Math.ceil(
            (this.subtotal - this.discountValue) * this.retefuenteVal
          );
        }
        if (this.reteica) {
          this.reteicaTotal = Math.ceil(
            (this.subtotal - this.discountValue) * this.reteicaVal
          );
        }
        this.total = Math.floor(
          (this.subtotal - this.discountValue) + this.ivaTotal - this.retefuenteTotal - this.reteicaTotal
        );
      });
    }
  }

  async ionViewDidEnter() {
    this.calculate();
  }

  ngOnInit() { }

  goToProductList() {
    this.nav.navigateForward('/tabs/products');
  }

  createOrder() {
    this.userData.getUserData().then((data) => {
      let currentNumber;
      const _data = data as BillData;
      if (_data.bill_current === '') {
        // tslint:disable-next-line: radix
        currentNumber = parseInt(_data.bill_from) + 1;
      } else {
        // tslint:disable-next-line: radix
        currentNumber = parseInt(_data.bill_current) + 1;
      }
      this.userData.setBillingNumber(currentNumber);
      this.stringVendor = _data.bill_prefix + '-' + currentNumber + ' / del ' + _data.bill_from + ' al ' + _data.bill_to;
      this.order.createOrder(false, _data.bill_prefix + '-' + currentNumber).then((data) => {
        this.prepareToPrint();
      });
    }).catch((err) => {
      console.log(err);
    });

  }

  noSpecialChars(string) {
    const translate = {
      à: 'a',
      á: 'a',
      â: 'a',
      ã: 'a',
      ä: 'a',
      å: 'a',
      æ: 'a',
      ç: 'c',
      è: 'e',
      é: 'e',
      ê: 'e',
      ë: 'e',
      ì: 'i',
      í: 'i',
      î: 'i',
      ï: 'i',
      ð: 'd',
      ñ: 'n',
      ò: 'o',
      ó: 'o',
      ô: 'o',
      õ: 'o',
      ö: 'o',
      ø: 'o',
      ù: 'u',
      ú: 'u',
      û: 'u',
      ü: 'u',
      ý: 'y',
      þ: 'b',
      ÿ: 'y',
      ŕ: 'r',
      À: 'A',
      Á: 'A',
      Â: 'A',
      Ã: 'A',
      Ä: 'A',
      Å: 'A',
      Æ: 'A',
      Ç: 'C',
      È: 'E',
      É: 'E',
      Ê: 'E',
      Ë: 'E',
      Ì: 'I',
      Í: 'I',
      Î: 'I',
      Ï: 'I',
      Ð: 'D',
      Ñ: 'N',
      Ò: 'O',
      Ó: 'O',
      Ô: 'O',
      Õ: 'O',
      Ö: 'O',
      Ø: 'O',
      Ù: 'U',
      Ú: 'U',
      Û: 'U',
      Ü: 'U',
      Ý: 'Y',
      Þ: 'B',
      Ÿ: 'Y',
      Ŕ: 'R',
    },
      translate_re = /[àáâãäåæçèéêëìíîïðñòóôõöøùúûüýþßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŕŕÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÝÝÞŸŔŔ]/gim;
    return string.replace(translate_re, function (match) {
      return translate[match];
    });
  }

  removeCustom(id) {
    this.order.removeProduct(id);
    this.calculate();
  }

  print(device, data) {
    console.log('Device mac: ', device);
    console.log('Data: ', data);
    this.loadCtrl
      .create({
        message: 'Imprimiendo...',
      })
      .then((load) => {
        load.present();
        this.printer.connectBluetooth(device).subscribe(
          (status) => {
            console.log(status);
            this.printer
              .printData(this.noSpecialChars(data))
              .then((printStatus) => {
                console.log(printStatus);
                load.dismiss();
                this.alertCtrl
                  .create({
                    message: 'Impreso correctamente!',
                    buttons: [
                      {
                        text: 'Ok',
                        handler: () => {
                          this.printer.disconnectBluetooth();
                        },
                      },
                    ],
                  })
                  .then((alert) => {
                    alert.present();
                  });
              })
              .catch((error) => {
                console.log(error);
                load.dismiss();
                this.alertCtrl
                  .create({
                    message:
                      'Hubo un error imprimiendo, porfavor inténtelo de nuevo!',
                    buttons: [
                      {
                        text: 'Ok',
                        handler: () => {
                          load.dismiss();
                          this.printer.disconnectBluetooth();
                        },
                      },
                    ],
                  })
                  .then((alert) => {
                    alert.present();
                  });
              });
          },
          (error) => {
            console.log(error);
            load.dismiss();
            const alert = this.alertCtrl
              .create({
                message:
                  'Hubo un error conectándose a la impresora, porfavor inténtelo de nuevo!',
                buttons: [
                  {
                    text: 'Ok',
                    handler: () => {
                      load.dismiss();
                      // this.printer.disconnectBluetooth();
                    },
                  },
                ],
              })
              .then((alert) => {
                alert.present();
              });
          }
        );
      });
  }

  prepareToPrint() {

    const data = { title: '', text: '', email: '' };
    data.title = 'Lácteos El Portillo LTDA';
    data.email = 'el.portillo@hotmail.com';
    data.text = 'NIT: 900364558-4';
    const _date = new Date();
    let receipt = '';
    receipt += commands.HARDWARE.HW_INIT;
    receipt += commands.TEXT_FORMAT.TXT_2WIDTH;
    receipt += commands.TEXT_FORMAT.TXT_ALIGN_CT;
    receipt += data.title.toUpperCase();
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_NORMAL;
    receipt += data.email;
    receipt += commands.EOL;
    receipt += data.text;
    receipt += commands.EOL;
    receipt += 'Transversal 80 B. 65 F 83 sur';
    receipt += commands.EOL;
    receipt += 'www.lacteoselportillo.com';
    receipt += commands.EOL;
    receipt += 'Cel: 320 3431841 - 7750439';
    receipt += commands.EOL;
    receipt += this.stringVendor;
    receipt += commands.EOL;
    receipt += commands.HORIZONTAL_LINE.HR_58MM;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
    receipt +=
      'Fecha: ' +
      _date.toLocaleTimeString() +
      ' - ' +
      _date.toLocaleDateString();
    receipt += commands.EOL;
    receipt += 'Cliente: ' + this.order.getCustomerData().first_name + ' ' + this.order.getCustomerData().last_name ;
    receipt += commands.EOL;
    receipt += 'CC/NIT: ' + this.document;
    receipt += commands.EOL;
    receipt += 'Direccion: ' + this.order.getCustomerData().billing.address_1;
    receipt += commands.EOL;
    receipt += commands.HORIZONTAL_LINE.HR2_58MM;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
    receipt += 'Producto                  Costo U.  Cant.  Total';
    this.order.getProductList().forEach((product) => {
      receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
      receipt += product.name;
      receipt += commands.EOL;
      receipt += commands.TEXT_FORMAT.TXT_ALIGN_RT;
      receipt += '$' + (product.regular_price * 1).toLocaleString('es-CO');
      receipt += '   ';
      receipt += '    ' + (product.totalCount * 1).toLocaleString('es-CO');
      receipt += '  ';
      receipt +=
        '$' +
        (product.totalCount * product.regular_price).toLocaleString('es-CO');
      receipt += commands.EOL;
    });
    receipt += commands.EOL;
    if (this.reteica) {
      receipt += 'Reteica $' + this.reteicaTotal;
      receipt += commands.EOL;
    }
    if (this.retefuente) {
      receipt += 'Retefuente $' + this.retefuenteTotal;
      receipt += commands.EOL;
    }
    receipt += 'IVA $' + this.ivaTotal;
    receipt += commands.EOL;
    receipt += 'Descuentos %' + this.couponDiscount;
    receipt += commands.EOL;
    receipt += 'Subtotal $' + this.subtotal;
    receipt += commands.EOL;
    receipt += 'Total $' + this.total;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_ALIGN_CT;
    receipt += commands.EOL;
    receipt += 'Copia';
    // secure space on footer
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    this.mountAlertBt(receipt);
  }

  getType(val) {
    return typeof val;
  }

  showToast(data) {
    const toast = this.toastCtrl
      .create({
        duration: 3000,
        message: data,
        position: 'bottom',
      })
      .then((toast) => toast.present());
  }

  async mountAlertBt(data) {
    this.receipt = data;
    await this.printer
      .enableBluetooth()
      .then(() => {
        this.printer
          .searchBluetooth()
          .then((devices) => {
            this.myPrinters = [];
            devices.forEach((device) => {
              console.log('Devices: ', JSON.stringify(device));
              this.myPrinters.push({
                name: 'printer',
                value: device.address,
                label: device.name,
                type: 'radio',
              });
            });
          })
          .catch((error) => {
            console.log(error);

            this.showToast(
              'Hubo un error conectando la impresora, inténtelo de nuevo!'
            );
            this.mountAlertBt(this.receipt);
          });
      })
      .catch((error) => {
        console.log(error);
        this.showToast('Error activando bluetooth, inténtelo de nuevo!');
        this.mountAlertBt(this.receipt);
      });

    const alert = await this.alertCtrl.create({
      message: 'Seleccione la impresora',
      inputs: this.myPrinters,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
        },
        {
          text: 'Seleccione',
          handler: (device) => {
            if (!device) {
              this.showToast('Seleccione una!');
              return false;
            }
            console.log(device);
            this.print(device, this.receipt);
          },
        },
      ],
    });
    await alert.present();
  }
}


export interface BillData {
  bill_from: string;
  bill_to: string;
  bill_current: string;
  bill_prefix: string;
}