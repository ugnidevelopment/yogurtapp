import { Component, OnInit } from '@angular/core';
import { OrderService } from '../../services/order/order.service';
import {
  NavController,
  LoadingController,
  AlertController,
  ToastController,
} from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { PrinterProvider } from 'src/app/providers/printer/printer';
import { commands } from './../../providers/printer/printer-commands';
import { WpRestApiService } from '../../services/wp-rest-api/wp-rest-api.service';

@Component({
  selector: 'app-quote',
  templateUrl: './quote.page.html',
  styleUrls: ['./quote.page.scss'],
})
export class QuotePage implements OnInit {
  receipt: any;
  inputData: any = {};
  myPrinters = [];
  total: any;
  subtotal = 0;
  couponDiscount = 0;
  discPromise;
  document = '';
  constructor(
    public order: OrderService,
    public wpRestApi: WpRestApiService,
    public nav: NavController,
    private printer: PrinterProvider,
    private alertCtrl: AlertController,
    private loadCtrl: LoadingController,
    private toastCtrl: ToastController,
    private storage: Storage
  ) { }

  haveCustomer(): boolean {
    if (this.order.getCustomerData() !== undefined) {
      return true;
    }
    return false;
  }

  checkCoupon(item) {
    return item.key === 'cupon';
  }

  checkDocument(item) {
    return item.key === 'cedula';
  }

  goToCustomerList() {
    this.nav.navigateForward('/tabs/quotecustomerlist');
  }

  calculate() {
    this.subtotal = 0;
    this.couponDiscount = 0;
    let totalLine = 0;
    if (this.haveCustomer()) {
      const discPromise = new Promise<number>((resolve, reject) => {
        this.storage.get('TOKEN').then((token) => {
          this.wpRestApi
            .getMembership(token.token)
            .then((data) => {
              this.document = this.order
                .getCustomerData()
                .meta_data.find(this.checkDocument).value;
              if (
                this.order.getCustomerData().meta_data.find(this.checkCoupon) !=
                undefined
              ) {
                const _userCoupon = this.order
                  .getCustomerData()
                  .meta_data.find(this.checkCoupon).value;
                const _coupons = data.filter((coupon) => {
                  if (_userCoupon.toUpperCase() == coupon.code.toUpperCase()) {
                    return coupon;
                  }
                });
                if (_coupons.length != 0) {
                  this.couponDiscount = _coupons[0].amount;
                }
                resolve(this.couponDiscount);
              }
            })
            .catch((err) => {
              console.log(err);
            });
        });
      });
      discPromise.then((resolve) => {
        const discount = 1 - resolve / 100;
        this.order.getProductList().forEach((product) => {
            totalLine += product.totalCount * product.regular_price;
            product.customLine = Math.ceil(
              product.totalCount * product.regular_price
            );
        });
        this.total = Math.ceil(totalLine);
        this.subtotal = Math.ceil(totalLine);
        this.total = Math.ceil(this.total * discount);
      });
    }
  }

  ionViewDidEnter() {
    console.log('entered page');
    this.calculate();
  }

  ngOnInit() { }

  goToProductList() {
    this.nav.navigateForward('/tabs/products');
  }

  createOrder() {
    this.order.createOrder(true).then((data) => {
      console.log(data);
      this.prepareToPrint();
    });
  }

  noSpecialChars(string) {
    const translate = {
      à: 'a',
      á: 'a',
      â: 'a',
      ã: 'a',
      ä: 'a',
      å: 'a',
      æ: 'a',
      ç: 'c',
      è: 'e',
      é: 'e',
      ê: 'e',
      ë: 'e',
      ì: 'i',
      í: 'i',
      î: 'i',
      ï: 'i',
      ð: 'd',
      ñ: 'n',
      ò: 'o',
      ó: 'o',
      ô: 'o',
      õ: 'o',
      ö: 'o',
      ø: 'o',
      ù: 'u',
      ú: 'u',
      û: 'u',
      ü: 'u',
      ý: 'y',
      þ: 'b',
      ÿ: 'y',
      ŕ: 'r',
      À: 'A',
      Á: 'A',
      Â: 'A',
      Ã: 'A',
      Ä: 'A',
      Å: 'A',
      Æ: 'A',
      Ç: 'C',
      È: 'E',
      É: 'E',
      Ê: 'E',
      Ë: 'E',
      Ì: 'I',
      Í: 'I',
      Î: 'I',
      Ï: 'I',
      Ð: 'D',
      Ñ: 'N',
      Ò: 'O',
      Ó: 'O',
      Ô: 'O',
      Õ: 'O',
      Ö: 'O',
      Ø: 'O',
      Ù: 'U',
      Ú: 'U',
      Û: 'U',
      Ü: 'U',
      Ý: 'Y',
      Þ: 'B',
      Ÿ: 'Y',
      Ŕ: 'R',
    },
      translate_re = /[àáâãäåæçèéêëìíîïðñòóôõöøùúûüýþßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŕŕÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÝÝÞŸŔŔ]/gim;
    return string.replace(translate_re, function (match) {
      return translate[match];
    });
  }

  removeCustom(id) {
    this.order.removeProduct(id);
    this.calculate();
  }

  print(device, data) {
    console.log('Device mac: ', device);
    console.log('Data: ', data);
    this.loadCtrl
      .create({
        message: 'Imprimiendo...',
      })
      .then((load) => {
        load.present();
        this.printer.connectBluetooth(device).subscribe(
          (status) => {
            console.log(status);
            this.printer
              .printData(this.noSpecialChars(data))
              .then((printStatus) => {
                console.log(printStatus);
                load.dismiss();
                this.alertCtrl
                  .create({
                    message: 'Impreso correctamente!',
                    buttons: [
                      {
                        text: 'Ok',
                        handler: () => {
                          this.printer.disconnectBluetooth();
                        },
                      },
                    ],
                  })
                  .then((alert) => {
                    alert.present();
                  });
              })
              .catch((error) => {
                console.log(error);
                load.dismiss();
                this.alertCtrl
                  .create({
                    message:
                      'Hubo un error imprimiendo, porfavor inténtelo de nuevo!',
                    buttons: [
                      {
                        text: 'Ok',
                        handler: () => {
                          load.dismiss();
                          this.printer.disconnectBluetooth();
                        },
                      },
                    ],
                  })
                  .then((alert) => {
                    alert.present();
                  });
              });
          },
          (error) => {
            console.log(error);
            load.dismiss();
            const alert = this.alertCtrl
              .create({
                message:
                  'Hubo un error conectándose a la impresora, porfavor inténtelo de nuevo!',
                buttons: [
                  {
                    text: 'Ok',
                    handler: () => {
                      load.dismiss();
                      // this.printer.disconnectBluetooth();
                    },
                  },
                ],
              })
              .then((alert) => {
                alert.present();
              });
          }
        );
      });
  }

  prepareToPrint() {
    // let total = 0;
    // u can remove this when generate the receipt using another method
    const data = { title: '', text: '', email: '' };
    data.title = 'Cotizacion El Portillo LTDA';
    const _date = new Date();
    let receipt = '';
    receipt += commands.HARDWARE.HW_INIT;
    receipt += commands.TEXT_FORMAT.TXT_2WIDTH;
    receipt += commands.TEXT_FORMAT.TXT_ALIGN_CT;
    receipt += data.title.toUpperCase();
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_NORMAL;
    receipt += data.email;
    receipt += commands.EOL;
    receipt += data.text;
    receipt += commands.EOL;
    receipt += commands.HORIZONTAL_LINE.HR_58MM;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
    receipt +=
      'Fecha: ' +
      _date.toLocaleTimeString() +
      ' - ' +
      _date.toLocaleDateString();
    receipt += commands.EOL;
    receipt += 'Cliente: ' + this.order.getCustomerData().first_name;
    receipt += commands.EOL;
    receipt += 'CC/NIT: ' + this.document;
    receipt += commands.EOL;
    receipt += commands.HORIZONTAL_LINE.HR2_58MM;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
    receipt += 'Producto                  Costo U.  Cant.  Total';
    this.order.getProductList().forEach((product) => {
      let totalLine = 0;
      if (product.tax_status == 'none') {
        totalLine = product.totalCount * product.regular_price;
      } else {
        totalLine = (product.totalCount * product.regular_price) / 1.19;
      }
      product.customLine = Math.ceil(totalLine);
      receipt += commands.TEXT_FORMAT.TXT_ALIGN_LT;
      receipt += product.name;
      receipt += commands.EOL;
      receipt += commands.TEXT_FORMAT.TXT_ALIGN_RT;
      receipt += '$' + (product.regular_price * 1).toLocaleString('es-CO');
      receipt += '   ';
      receipt += '    ' + (product.totalCount * 1).toLocaleString('es-CO');
      receipt += '  ';
      receipt += '$' + product.customLine.toLocaleString('es-CO');
      receipt += commands.EOL;
    });
    receipt += commands.EOL;
    receipt += 'Descuentos %' + this.couponDiscount;
    receipt += commands.EOL;
    receipt += 'Subtotal $' + this.subtotal;
    receipt += commands.EOL;
    receipt += 'Total $' + this.total;
    receipt += commands.EOL;
    receipt += commands.TEXT_FORMAT.TXT_ALIGN_CT;
    receipt += 'Transversal 80 B. 65 F 83 sur.  Tel 7750439. -. 320 3431841';
    // secure space on footer
    receipt += commands.EOL;
    receipt += commands.EOL;
    receipt += commands.EOL;
    this.mountAlertBt(receipt);
  }

  getType(val) {
    return typeof val;
  }

  showToast(data) {
    const toast = this.toastCtrl
      .create({
        duration: 3000,
        message: data,
        position: 'bottom',
      })
      .then((toast) => toast.present());
  }

  async mountAlertBt(data) {
    this.receipt = data;
    await this.printer
      .enableBluetooth()
      .then(() => {
        this.printer
          .searchBluetooth()
          .then((devices) => {
            devices.forEach((device) => {
              console.log('Devices: ', JSON.stringify(device));
              this.myPrinters.push({
                name: 'printer',
                value: device.address,
                label: device.name,
                type: 'radio',
              });
            });
          })
          .catch((error) => {
            console.log(error);

            this.showToast(
              'Hubo un error conectando la impresora, inténtelo de nuevo!'
            );
            this.mountAlertBt(this.receipt);
          });
      })
      .catch((error) => {
        console.log(error);
        this.showToast('Error activando bluetooth, inténtelo de nuevo!');
        this.mountAlertBt(this.receipt);
      });

    const alert = await this.alertCtrl.create({
      message: 'Seleccione la impresora',
      inputs: this.myPrinters,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
        },
        {
          text: 'Seleccione',
          handler: (device) => {
            if (!device) {
              this.showToast('Seleccione una!');
              return false;
            }
            console.log(device);
            this.print(device, this.receipt);
          },
        },
      ],
    });
    await alert.present();
  }
}
