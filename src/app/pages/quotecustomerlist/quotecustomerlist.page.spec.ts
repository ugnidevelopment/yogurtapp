import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { QuoteCustomerlistPage } from "./quotecustomerlist.page";

describe("QuoteCustomerlistPage", () => {
  let component: QuoteCustomerlistPage;
  let fixture: ComponentFixture<QuoteCustomerlistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [QuoteCustomerlistPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuoteCustomerlistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
