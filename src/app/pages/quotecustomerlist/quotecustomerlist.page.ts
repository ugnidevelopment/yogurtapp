import { Component, OnInit } from "@angular/core";
import { WpRestApiService } from "../../services/wp-rest-api/wp-rest-api.service";
import { NavController } from "@ionic/angular";
import { UserdataService } from "../../services/userdata/userdata.service";
import { Router } from "@angular/router";
import { OrderService } from "../../services/order/order.service";
import { Storage } from "@ionic/storage";
import { LoadingService } from "../../services/loading/loading.service";

@Component({
  selector: "app-customerlist",
  templateUrl: "./quotecustomerlist.page.html",
  styleUrls: ["./quotecustomerlist.page.scss"],
})
export class QuoteCustomerlistPage implements OnInit {
  public customerList: any;
  public showList: boolean = false;

  constructor(
    public wpRestApi: WpRestApiService,
    public nav: NavController,
    public userData: UserdataService,
    public router: Router,
    public order: OrderService,
    private storage: Storage,
    private loading: LoadingService
  ) {
    this.loading.present("Cargando clientes...");
    this.getCustomers();
  }

  ngOnInit() {}

  getCustomers() {
    this.storage.get("TOKEN").then((token) => {
      this.wpRestApi
        .getWordpressUserByRole("cliente", token.token)
        .then((data) => {
          this.customerList = data;
          this.makeDocument();
          this.showList = true;
          this.loading.dismiss();
        })
        .catch((err) => {
          console.log(err);
        });
    });
  }

  checkCC(item) {
    return item.key == "cedula";
  }

  makeDocument() {
    this.customerList.forEach((customer) => {
      if (customer.meta_data.find(this.checkCC) != undefined) {
        customer.cc = customer.meta_data.find(this.checkCC).value;
      }
    });
  }

  setCustomerToOrder(customer: any) {
    this.order.setCustomerData(customer);
    this.goToCreateOrder();
    console.log(this.order.getCustomerData());
  }

  goToCreateOrder() {
    this.nav.navigateForward("/tabs/quote");
  }

  doRefresh(event) {
    this.storage.get("TOKEN").then((token) => {
      this.wpRestApi
        .getWordpressUserByRole("cliente", token.token)
        .then((data) => {
          this.customerList = data;
          this.showList = true;
          event.target.complete();
        })
        .catch((err) => {
          console.log(err);
        });
    });
  }
}
